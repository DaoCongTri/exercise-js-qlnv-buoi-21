function renderArrEmployee(ArrEmployee) {
  var contentHTML = "";
  for (var i = 0; i < ArrEmployee.length; i++) {
    var employee = ArrEmployee[i];
    var contentTr = `
        <tr>
            <td>${employee.account}</td>
            <td>${employee.name}</td>
            <td>${employee.email}</td>
            <td>${employee.date}</td>
            <td>${employee.job}</td>
            <td>${employee.totalSalary()}</td>
            <td>${employee.grade()}</td>
            <td>
              <button class="btn btn-success"
                data-toggle="modal" data-target="#myModal"
                onclick="suaNV(${employee.account})">Sửa</button>
              <button class="btn btn-danger" onclick="xoaNV(${
                employee.account
              })">Xóa</button>
                
            </td>
        </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
function layThongTinTuForm() {
  var account = document.getElementById("tknv").value;
  var name = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var date = new Date(document.getElementById("datepicker").value);
  var salary = document.getElementById("luongCB").value * 1;
  var job = document.getElementById("chucvu").value;
  var time = document.getElementById("gioLam").value * 1;
  var employee = new nhanVien(
    account,
    name,
    email,
    password,
    date,
    salary,
    job,
    time
  );
  return employee;
}
function showThongTinLenForm(employee) {
  document.getElementById("tknv").value = employee.account;
  document.getElementById("name").value = employee.name;
  document.getElementById("email").value = employee.email;
  document.getElementById("password").value = employee.password;
  document.getElementById("datepicker").value = employee.date;
  document.getElementById("luongCB").value = employee.salary;
  document.getElementById("chucvu").value = employee.job;
  document.getElementById("gioLam").value = employee.time;
}
