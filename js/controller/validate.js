var showMessage = function (id, message) {
  document.getElementById(id).innerHTML = message;
};
var kiemTraTKTrung = function (account, ArrEmployee) {
  var index = ArrEmployee.findIndex((item) => {
    return account == item.account;
  });
  if (index == -1) {
    showMessage("tbTKNV", "");
    return true;
  } else {
    showMessage("tbTKNV", `<p>Mã tài khoản bạn nhập bị trùng</p>`);
    return false;
  }
};
var kiemTraEmailTrung = function (email, ArrEmployee) {
  var index = ArrEmployee.findIndex((item) => {
    return email == item.email;
  });
  if (index == -1) {
    showMessage("tbEmail", "");
    return true;
  } else {
    showMessage("tbEmail", `<p>Email bạn nhập bị trùng</p>`);
    return false;
  }
};
var kiemTraTK = function (account) {
  if (/^[0-9]{4,6}$/.test(account)) {
    showMessage("tbTKNV", "");
    return true;
  }else {
    showMessage("tbTKNV", "Mã tài khoản nhân viên chỉ được nhập 4-6 ký số!");
    return false;
  };
};
var kiemTraTen = function (name) {
  if (/^[A-Za-z ]+$/.test(name)) {
    showMessage("tbHoTen", "");
    return true;
  } else {
    showMessage("tbHoTen", "Tên nhân viên chỉ được nhập chữ!");
    return false;
  }
};
var kiemTraEmail = function (email) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (re.test(email)) {
    showMessage("tbEmail", "");
    return true;
  } else {
    showMessage("tbEmail", "Email của nhân viên phải hợp lệ!");
    return false;
  }
};
var kiemTraMK = function (password) {
  const mk = /^(?=.*\d)(?=.*[A-Z])(?=.*\W)[\dA-Za-z\W]{6,10}$/;
  if (mk.test(password)) {
    showMessage("tbMatKhau", "");
    return true;
  } else {
    showMessage(
      "tbMatKhau",
      "Mật khẩu bắt buộc 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
    );
    return false;
  };
};
var kiemTraLuong = function (salary){
    if (salary >= 1000000 && salary <= 20000000) {
        showMessage("tbLuongCB","");
        return true;
    }else{
        showMessage("tbLuongCB", "Lương cơ bản nhân viên phải từ 1.000.000 - 20.000.000!");
        return false;
    };
};
var kiemTraGioLam = function (time){
    if (time >= 80 && time <= 200) {
        showMessage("tbGiolam" ,"");
        return true;
    }else {
        showMessage("tbGiolam", "Số giờ làm trong tháng nhân viên phải từ 80 - 200!");
        return false;   
    };
};
var kiemTraRong = function (idErr, value) {
  if (value.length == 0 || value == 0) {
    showMessage(idErr, "Mục này không được bỏ trống! Vui lòng nhập");
    return false;
  }else {
    showMessage(idErr, "");
    return true;
  }
};
