var ArrEmployee = [];
var dataJson = localStorage.getItem("DSSV_LOCAL");
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var employee = new nhanVien(
      item.account,
      item.name,
      item.email,
      item.password,
      item.date,
      item.salary,
      item.job,
      item.time
    );
    ArrEmployee.push(employee);
  }
  renderArrEmployee(ArrEmployee);
}
function themNhanVien() {
  var employee = layThongTinTuForm();
  var isValid =
    kiemTraRong("tbTKNV", employee.account) &&
    kiemTraTK(employee.account) &&
    kiemTraTKTrung(employee.account, ArrEmployee);
  isValid =
    isValid & kiemTraRong("tbHoTen", employee.name) &&
    kiemTraTen(employee.name);
  isValid =
    isValid & kiemTraRong("tbEmail", employee.email) &&
    kiemTraEmail(employee.email) &&
    kiemTraEmailTrung(employee.email, ArrEmployee);
  isValid =
    isValid & kiemTraRong("tbMatKhau", employee.password) &&
    kiemTraMK(employee.password);
  isValid = isValid & kiemTraRong("tbNgay", employee.date);
  isValid =
    isValid & kiemTraRong("tbLuongCB", employee.salary) &&
    kiemTraLuong(employee.salary);
  isValid = isValid & kiemTraRong("tbChucVu", employee.job);
  isValid =
    isValid & kiemTraRong("tbGiolam", employee.time) &&
    kiemTraGioLam(employee.time);
  if (isValid) {
    ArrEmployee.push(employee);
    var dataJson = JSON.stringify(ArrEmployee);
    localStorage.setItem("DSSV_LOCAL", dataJson);
  }
  renderArrEmployee(ArrEmployee);
  console.log(ArrEmployee);
}
function capNhatNhanVien() {
  var employee = layThongTinTuForm();
  var isValid =
  kiemTraRong("tbHoTen", employee.name) &&
  kiemTraTen(employee.name);
  isValid =
    isValid & kiemTraRong("tbEmail", employee.email) &&
    kiemTraEmail(employee.email);
  isValid =
    isValid & kiemTraRong("tbMatKhau", employee.password) &&
    kiemTraMK(employee.password);
  isValid = isValid & kiemTraRong("tbNgay", employee.date);
  isValid =
    isValid & kiemTraRong("tbLuongCB", employee.salary) &&
    kiemTraLuong(employee.salary);
  isValid = isValid & kiemTraRong("tbChucVu", employee.job);
  isValid =
    isValid & kiemTraRong("tbGiolam", employee.time) &&
    kiemTraGioLam(employee.time);
  document.getElementById("tknv").disabled = false;
  var vitri = ArrEmployee.findIndex((item) => {
    return item.account == employee.account;
  });
  if (vitri !== -1) {
    if (isValid) {
      ArrEmployee[vitri] = employee;
      renderArrEmployee(ArrEmployee);
      resetForm();
      var dataJson = JSON.stringify(ArrEmployee);
      localStorage.setItem("DSSV_LOCAL", dataJson);
    };
  };
};
function xoaNV(id) {
  var vitri = -1;
  for (var i = 0; i < ArrEmployee.length; i++) {
    var employee = ArrEmployee[i];
    if (employee.account == id) {
      vitri = i;
      break;
    }
  }
  ArrEmployee.splice(vitri, 1);
  renderArrEmployee(ArrEmployee);
}
function suaNV(id) {
  var vitri = ArrEmployee.findIndex((item) => {
    return item.account == id;
  });
  if (vitri != -1) {
    showThongTinLenForm(ArrEmployee[vitri]);
    document.getElementById("tknv").disabled = true;
    return true;
  };
}
function resetForm() {
  document.getElementById("formQLNV").reset();
}
function searchNV() {
  var request = document.getElementById("searchName").value.trim();
  document.getElementById("searchName").value = "";
  var result = ArrEmployee.filter(function(e){
    return e.grade() === request;
  });
  renderArrEmployee(result);
};