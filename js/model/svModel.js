function nhanVien(_acc, _name, _email, _password, _date, _salary, _job, _time){
    this.account = _acc;
    this.name = _name;
    this.email = _email;
    this.password = _password;
    this.date = _date;
    this.salary = _salary;
    this.job = _job;
    this.time = _time;
    this.totalSalary = function(){
        var sum = 0;
        switch (this.job) {
            case "Sếp":
                sum = this.salary * 3;
                break;
            case "Trưởng phòng":
                sum = this.salary * 2;
                break;
            case "Nhân viên":
                sum = this.salary * 1;
                break;
            default:
                break;
        }
        return sum;
    };
    this.grade = function(){
        if (this.time >= 192) {
            return "Xuất Xắc";
        } else if(this.time >= 176){
            return "Giỏi";
        }else if(this.time >= 160){
            return "Khá";
        }else{
            return "Trung Bình";
        };
    }; 
};